package com.example.blogbackend.Controller;

import com.example.blogbackend.Entity.Comment;
import com.example.blogbackend.repository.CommentRepo;
import java.time.LocalDate;
import java.util.List;
import javax.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/comment")
@Validated
public class CommentController {

	@Autowired
	CommentRepo commentRepo;

	// CREATE
	@PostMapping()
	public Comment save(@RequestBody Comment comment ) {
		comment.setDate(LocalDate.now());
		if (!commentRepo.save(comment)) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		return commentRepo.findById(comment.getId());
	}

	// READ
	@GetMapping()
	public List<Comment> findAll() {
			return commentRepo.findAll();
	}

	@GetMapping("/{id}")
	public Comment findById(@Min(1) @PathVariable int id ) {
    Comment comment = commentRepo.findById(id);
    if (comment == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	return comment;
	}

	@GetMapping("/article/{id}")
	public List<Comment> finbByArticleID(@Min(1) @PathVariable int id) {
		return commentRepo.FindByArticleId(id);
	}
	// UPDATE
	@PutMapping("/{id}")
	public Comment update(@RequestBody Comment comment, @Min(1) @PathVariable int id) {
		if(comment.getId() != id) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		if (!commentRepo.updateById(comment)) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		return commentRepo.findById(comment.getId());
	}
	@PatchMapping("/{id}")
	public Comment patch(@RequestBody Comment comment, @Min(1) @PathVariable int id) {
		Comment baseComment = commentRepo.findById(id);
		if (comment.getAuthor() != null) baseComment.setAuthor(comment.getAuthor());
		if (comment.getContent() != null) baseComment.setContent(comment.getContent());
		if (comment.getDate() != null) baseComment.setDate(comment.getDate());
		if (!commentRepo.updateById(baseComment)) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		return commentRepo.findById(id);
	}

	// DELETE
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@Min(1) @PathVariable int id) {
		if (!commentRepo.delete(id)) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	}
}
