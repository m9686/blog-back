  package com.example.blogbackend.Controller;

import com.example.blogbackend.Entity.Article;
import com.example.blogbackend.repository.ArticleRepo;
import java.time.LocalDate;
import java.util.List;
import javax.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/article")
@Validated
public class ArticleController {

	@Autowired
	ArticleRepo articleRepo;

	// CREATE
	@PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
	public Article save(@RequestBody Article article ) {
		article.setDate(LocalDate.now());
		if (!articleRepo.save(article)) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		return article;
	}
	// READ
	@GetMapping()
	public List<Article> findAll() {
		return articleRepo.findAll();
	}
	@GetMapping("/{id}")
	public Article findById(@Min(1) @PathVariable int id) {
    Article article = articleRepo.findById(id);
    if (article == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		return article;
	}
	// UPDATE
	@PutMapping("/{id}")
	public Article update(@RequestBody Article article, @Min(1) @PathVariable int id) {
		if (article.getId() != id) throw new ResponseStatusException((HttpStatus.BAD_REQUEST));
		if (!articleRepo.updateById(article)) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		return articleRepo.findById(id);
	}
	@PatchMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Article patch(@RequestBody Article article,@Min(1) @PathVariable int id) {
		Article baseArticle = articleRepo.findById(id);
		if (article.getTitle() != null) baseArticle.setTitle(article.getTitle());
		if (article.getAuthor() != null) baseArticle.setAuthor(article.getAuthor());
		if (article.getContent() != null) baseArticle.setContent(article.getContent());
		if (article.getDate() != null) baseArticle.setDate(article.getDate());
		if (!articleRepo.updateById(baseArticle)) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		return articleRepo.findById(id);
	}
	// DELETE
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@Min(1)@PathVariable int id) {;
		if (!articleRepo.delete(id)) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	}
}

