package com.example.blogbackend.repository;

import com.example.blogbackend.Entity.Article;
import com.example.blogbackend.Entity.Comment;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ArticleRepo {

	@Autowired
	private DataSource datasource;
	@Autowired
	private CommentRepo commentRepo;

	// CREATE
	public boolean save(Article article) {
		try (Connection connection = datasource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					"INSERT INTO article (title, author, content, date) " +
							"VALUES (?, ?, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS
			);
			stmt.setString(1, article.getTitle());
			stmt.setString(2, article.getAuthor());
			stmt.setString(3, article.getContent());
			stmt.setDate(4, Date.valueOf(article.getDate()));
			boolean done = stmt.executeUpdate() == 1;
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				article.setId(rs.getInt(1));
			}
			return done;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Database access error");
		}
	}
	// READ
	public List<Article> findAll() {
		List<Article> articleList = new ArrayList<>();
		Article newArticle;

		try (Connection connection = datasource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					"SELECT * FROM article"
			);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				newArticle = new Article(
						rs.getInt("id"),
						rs.getString("title"),
						rs.getString("author"),
						rs.getString("content"),
						rs.getDate("date").toLocalDate(),
						commentRepo.FindByArticleId(rs.getInt("id"))
				);
				articleList.add(newArticle);
			}
			return articleList;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Database access error");
		}
	}
	public Article findById(int id) {
		Article newArticle = null;
		List<Comment> newList = new ArrayList<>();
		try (Connection connection = datasource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					"SELECT a.id AS ai, a.title AS at, a.author AS aa, a.content AS ac, a.date AS ad, "
							+ "c.id AS ci, c.id_article AS cia, c.author AS ca, c.content AS cc, c.date AS cd "
							+ "FROM article AS a "
							+ "LEFT JOIN comment c "
							+ "ON a.id = c.id_article "
							+ "WHERE a.id = ?"
			);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while (rs.next()) {
				if (count == 0) {
					newArticle = new Article(
							rs.getInt("ai"),
							rs.getString("at"),
							rs.getString("aa"),
							rs.getString("ac"),
							rs.getDate("ad").toLocalDate()
					);
				}
				if (rs.getInt("ci") != 0) {
					Comment newComment = new Comment(
							rs.getInt("ci"),
							rs.getInt("cia"),
							rs.getString("ca"),
							rs.getString("cc"),
							rs.getDate("cd").toLocalDate()
					);
					newList.add(newComment);
				}
				count++;
			}
      if (newArticle != null) {
        newArticle.setLisComment(newList);
      }
			return newArticle;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Database access error");
		}
	}

	// UPDATE
	public boolean updateById(Article article) {
		try(Connection connection = datasource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement("UPDATE article "
					+ "SET title = ?,"
					+ "author = ?,"
					+ "content = ?,"
					+ "date = ?"
					+ "WHERE id = ?;"
			);
			stmt.setString(1, article.getTitle());
			stmt.setString(2, article.getAuthor());
			stmt.setString(3, article.getContent());
			stmt.setDate(4, Date.valueOf(article.getDate()));
			stmt.setInt(5, article.getId());

			return stmt.executeUpdate() == 1;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Database access error");
		}
	}
	// DELETE
	public boolean delete(int id) {
		try (Connection connection = datasource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					" DELETE FROM article WHERE id = ?"
			);
			stmt.setInt(1, id);
      boolean done = stmt.executeUpdate() == 1;
      System.out.println(done);
			return done;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Database access error");
		}
	}
}

