package com.example.blogbackend.repository;

import com.example.blogbackend.Entity.Comment;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CommentRepo {

	@Autowired
	private DataSource datasource;

	// CREATE
	public boolean save(Comment comment) {
		try (Connection connection = datasource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					"INSERT INTO comment (id_article, author, content, date) " +
							"VALUES (?, ?, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS
			);
			stmt.setInt(1, comment.getId_article());
			stmt.setString(2, comment.getAuthor());
			stmt.setString(3, comment.getContent());
			stmt.setDate(4, Date.valueOf(comment.getDate()));
			boolean done = stmt.executeUpdate() == 1;
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				comment.setId(rs.getInt(1));
			}
			return done;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Database access error");
		}
	}
	// READ
	public List<Comment> findAll() {
		List<Comment> commentList = new ArrayList<>();
		Comment newComment;

		try (Connection connection = datasource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					"SELECT * FROM comment"
			);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				newComment = new Comment(
						rs.getInt("id"),
						rs.getInt("id_article"),
						rs.getString("author"),
						rs.getString("content"),
						rs.getDate("date").toLocalDate()
				);
				commentList.add(newComment);
			}
			return commentList;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Database access error");
		}
	}
	public List<Comment> FindByArticleId(int id) {
		List<Comment> commentList = new ArrayList<>();
		Comment newComment;

		try (Connection connection = datasource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					"SELECT * FROM comment WHERE id_article = ?"
			);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				newComment = new Comment(
						rs.getInt("id"),
						rs.getInt("id_article"),
						rs.getString("author"),
						rs.getString("content"),
						rs.getDate("date").toLocalDate()
				);
				commentList.add(newComment);
			}
			return commentList;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Database access error");
		}
	}

	public Comment findById(int id) {
		List<Comment> commentList = new ArrayList<>();
		Comment newComment = new Comment();

		try (Connection connection = datasource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					"SELECT * FROM comment WHERE id= ?"
			);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				newComment = new Comment(
						rs.getInt("id"),
						rs.getInt("id_article"),
						rs.getString("author"),
						rs.getString("content"),
						rs.getDate("date").toLocalDate()
				);
				return newComment;
			}
			return null;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Database access error");
		}
	}
	// UPDATE
	public boolean updateById(Comment comment) {
		try (Connection connection = datasource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement("UPDATE comment "
					+ "SET author = ?,"
					+ "content = ?,"
					+ "date = ? "
					+ "WHERE id = ?"
			);
			stmt.setString(1, comment.getAuthor());
			stmt.setString(2, comment.getContent());
			stmt.setDate(3, Date.valueOf(comment.getDate()));
			stmt.setInt(4, comment.getId());

			return stmt.executeUpdate() == 1;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Database access error");
		}
	}
	// DELETE
	public boolean delete(int id) {
		try (Connection connection = datasource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					" DELETE FROM comment WHERE id = ?"
			);
			stmt.setInt(1, id);
			return stmt.executeUpdate() == 1;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Database access error");
		}
	}
}



