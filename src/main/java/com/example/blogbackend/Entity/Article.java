package com.example.blogbackend.Entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;

public class Article {

	private int id;
	@NotBlank
	private String title;
	@NotBlank
	private String author;
	@NotBlank
	private String content;
	@PastOrPresent
	private LocalDate date;
	private List<Comment> lisComment = new ArrayList<>();


	public Article() {
	}

	public Article(String title, String author, String content, LocalDate date) {
		this.title = title;
		this.author = author;
		this.content = content;
		this.date = date;
	}

	public Article(int id, String title, String author, String content, LocalDate date) {
		this.id = id;
		this.title = title;
		this.author = author;
		this.content = content;
		this.date = date;
	}

	public Article(String title, String author, String content, LocalDate date,
			List<Comment> lisComment) {
		this.title = title;
		this.author = author;
		this.content = content;
		this.date = date;
		this.lisComment = lisComment;
	}

	public Article(int id, String title, String author, String content, LocalDate date,
			List<Comment> lisComment) {
		this.id = id;
		this.title = title;
		this.author = author;
		this.content = content;
		this.date = date;
		this.lisComment = lisComment;
	}

	public Article(String title, String author, String content) {
		this.title = title;
		this.author = author;
		this.content = content;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public List<Comment> getLisComment() {
		return lisComment;
	}

	public void setLisComment(List<Comment> lisComment) { 
    if (lisComment != null) {
      this.lisComment = lisComment; 
    } else {
      this.lisComment = lisComment;
    }
  }
}
