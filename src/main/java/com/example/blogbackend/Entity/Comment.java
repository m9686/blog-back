package com.example.blogbackend.Entity;

import java.time.LocalDate;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

public class Comment {

	private int id;
	@NotNull
	private int id_article;
	@NotBlank
	private String author;
	@NotBlank
	private String content;
	@PastOrPresent
	private LocalDate date;

	public Comment() {
	}

	public Comment(int id_article, String author, String content, LocalDate date) {
		this.id_article = id_article;
		this.author = author;
		this.content = content;
		this.date = date;
	}

	public Comment(int id, int id_article, String author, String content, LocalDate date) {
		this.id = id;
		this.id_article = id_article;
		this.author = author;
		this.content = content;
		this.date = date;
	}

	public Comment(int id_article, String author, String content) {
		this.id_article = id_article;
		this.author = author;
		this.content = content;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_article() {
		return id_article;
	}

	public void setId_article(int id_article) {
		this.id_article = id_article;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}
}
