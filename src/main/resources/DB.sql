DROP TABLE comment;
DROP TABLE article;

CREATE TABLE article(
                        id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
                        title VARCHAR(128),
                        author VARCHAR(128),
                        content TEXT,
                        date DATE
);

CREATE TABLE comment(
                        id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
                        id_article INT UNSIGNED,
                        FOREIGN KEY (id_article) REFERENCES article(id)
                            ON DELETE CASCADE,
                        author VARCHAR(128),
                        content TEXT,
                        date DATE
);

INSERT INTO article(title, author, content, date) VALUES
                                                      ("Les Lapins", "Louis Pelu",
                                                       "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Urna id volutpat lacus laoreet non curabitur gravida arcu ac. Egestas quis ipsum suspendisse ultrices. Justo eget magna fermentum iaculis eu non diam phasellus vestibulum. Quam quisque id diam vel. Facilisi morbi tempus iaculis urna id volutpat lacus laoreet non. Bibendum arcu vitae elementum curabitur. Enim sed faucibus turpis in. Leo duis ut diam quam nulla. Augue mauris augue neque gravida in fermentum et sollicitudin. Eget velit aliquet sagittis id consectetur. Nunc sed id semper risus in hendrerit gravida rutrum. Faucibus purus in massa tempor nec feugiat nisl. Tellus elementum sagittis vitae et leo duis. In ante metus dictum at tempor commodo. Sem et tortor consequat id porta nibh. Lectus nulla at volutpat diam ut venenatis tellus in metus. Placerat orci nulla pellentesque dignissim enim sit.",
                                                       "2022-06-13"),
                                                      ("Les Ours", "Elise Leplein",
                                                       "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Urna id volutpat lacus laoreet non curabitur gravida arcu ac. Egestas quis ipsum suspendisse ultrices. Justo eget magna fermentum iaculis eu non diam phasellus vestibulum. Quam quisque id diam vel. Facilisi morbi tempus iaculis urna id volutpat lacus laoreet non. Bibendum arcu vitae elementum curabitur. Enim sed faucibus turpis in. Leo duis ut diam quam nulla. Augue mauris augue neque gravida in fermentum et sollicitudin. Eget velit aliquet sagittis id consectetur. Nunc sed id semper risus in hendrerit gravida rutrum. Faucibus purus in massa tempor nec feugiat nisl. Tellus elementum sagittis vitae et leo duis. In ante metus dictum at tempor commodo. Sem et tortor consequat id porta nibh. Lectus nulla at volutpat diam ut venenatis tellus in metus. Placerat orci nulla pellentesque dignissim enim sit.",
                                                       "2022-04-18"),
                                                      ("Les Oiseaux", "Pierre Bonnassier",
                                                       "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Urna id volutpat lacus laoreet non curabitur gravida arcu ac. Egestas quis ipsum suspendisse ultrices. Justo eget magna fermentum iaculis eu non diam phasellus vestibulum. Quam quisque id diam vel. Facilisi morbi tempus iaculis urna id volutpat lacus laoreet non. Bibendum arcu vitae elementum curabitur. Enim sed faucibus turpis in. Leo duis ut diam quam nulla. Augue mauris augue neque gravida in fermentum et sollicitudin. Eget velit aliquet sagittis id consectetur. Nunc sed id semper risus in hendrerit gravida rutrum. Faucibus purus in massa tempor nec feugiat nisl. Tellus elementum sagittis vitae et leo duis. In ante metus dictum at tempor commodo. Sem et tortor consequat id porta nibh. Lectus nulla at volutpat diam ut venenatis tellus in metus. Placerat orci nulla pellentesque dignissim enim sit.",
                                                       "2022-08-26");

INSERT INTO comment(id_article, author, content, date) VALUES
                                                           (1, "Pauline Lemur",
                                                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                                                            "2022-07-01"),
                                                           (1, "Jean Laglade",
                                                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                                                            "2022-07-03"),
                                                           (3, "Louis Pelu",
                                                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                                                            "2022-08-26"),
                                                           (3, "Pauline Lemur",
                                                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                                                            "2022-08-28"),
                                                           (3, "Anais Babtou",
                                                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                                                            "2022-08-29");
