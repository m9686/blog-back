package com.example.blogbackend.Controller;


import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.example.blogbackend.Entity.Article;
import com.example.blogbackend.Entity.Comment;
import com.example.blogbackend.repository.ArticleRepo;
import com.example.blogbackend.repository.CommentRepo;
import org.springframework.http.MediaType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
// !!!!!!
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
// !!!!!!

@WebMvcTest(controllers = ArticleController.class)
public class ArticleControllerTest {

  @Autowired
  MockMvc mvc;

  @MockBean
  public ArticleRepo articleRepo;

  @MockBean
  public CommentRepo commentRepo;

  List<Comment> comments = new ArrayList<Comment>();

  List<Article> testData = List.of(
    new Article(1, "Loutre", "Loic", "Ipsum", LocalDate.parse("2021-07-12")),
    new Article(2, "Panda", "Thomas", "Poupi", LocalDate.parse("2021-03-11")),
    new Article(3, "Canard", "Lucas", "Loulou", LocalDate.parse("2021-06-15"))
  );

  Article soloArticle = new Article(1, "Loutre", "Loic", "Ipsum", LocalDate.parse("2021-07-12"));
  

  @Test
  public void testDelete() throws Exception {
    when(articleRepo.delete(1)).thenReturn(true);
    mvc.perform(delete("/article/1"))
      .andExpect(status().isNoContent());
    verify(articleRepo, times(1)).delete(1);
  }

  @Test
  public void TestDeleteNotFound() throws Exception {
    when(articleRepo.delete(2)).thenReturn(false);
    mvc.perform(delete("/article/2"))
      .andExpect(status().isNotFound());
    verify(articleRepo, times(1)).delete(2);
  }

  @Test
  void testFindAll() throws Exception {
    when(articleRepo.findAll()).thenReturn(testData);
    mvc.perform(get("/article"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$").isArray())
      .andExpect(jsonPath("$[0].title").value("Loutre"));
    verify(articleRepo, times(1)).findAll();
  }

  @Test
  void testFindById() throws Exception {
    when(articleRepo.findById(1)).thenReturn(testData.get(0));
    mvc.perform(get("/article/1"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.author").value("Loic"))
      .andExpect(jsonPath("$.lisComment").isArray());
    verify(articleRepo, times(1)).findById(1);
  }

  @Test
  void testByIdNotFound() throws Exception {
    when(articleRepo.findById(56)).thenReturn(null);
    mvc.perform(get("/article/56"))
      .andExpect(status().isNotFound());
    verify(articleRepo, times(1)).findById(56);
  }

  @Test
  void testSave() throws Exception {
    when(articleRepo.save(soloArticle)).thenReturn(true);
    mvc.perform(post("/article")
      .contentType(MediaType.APPLICATION_JSON)
      .content("""
                {
                "title": "title",
                "author": "author",
                "content": "content"
                }
          """))
        .andExpect(status().isCreated()); 
    verify(articleRepo, times(1)).save(soloArticle);
  }

  @Test
  void testSaveUnprocessed() throws Exception {
    mvc.perform(post("/article")
      .contentType(MediaType.APPLICATION_JSON)
      .content("""
              {
                "title": "",
                "author": "author",
                "content": "content"
              }
          """))
        .andExpect(status().isBadRequest()); 
  }

  @Test
  void testUpdate() throws Exception {
    when(articleRepo.updateById(testData.get(0))).thenReturn(true);
    when(articleRepo.findById(1)).thenReturn(testData.get(0));
    mvc.perform(put("/article/1")
      .contentType(MediaType.APPLICATION_JSON)
      .content("""
              {
                "id": 1,
                "title": "title",
                "author": "author",
                "content": "content"
              }
            """))
      .andExpect(status().isOk());
    verify(articleRepo, times(1)).updateById(testData.get(0));
    verify(articleRepo, times(1)).findById(1);
    
  }

@Test
void testUpdateNotFound() throws Exception {
  when(articleRepo.findById(56)).thenReturn(null);

  mvc.perform(put("/article/56")
    .contentType(MediaType.APPLICATION_JSON)
    .content("""
            {
              "id": 56,
              "title": "title",
              "author": "author",
              "content": "content"
            }
          """))
    .andExpect(status().isNotFound());
  }


@Test
void testUpdateBadRequest() throws Exception {
  when(articleRepo.findById(56)).thenReturn(null);
  mvc.perform(put("/article/2")
    .contentType(MediaType.APPLICATION_JSON)
    .content("""
            {
              "id": 56,
              "title": "title",
              "author": "author",
              "content": "content"
            }
          """))
    .andExpect(status().isBadRequest());
  }

@Test
void testPatch() throws Exception {
  when(articleRepo.findById(1)).thenReturn(testData.get(0));
  when(articleRepo.updateById(testData.get(0))).thenReturn(true);
  mvc.perform(patch("/article/1")
    .contentType(MediaType.APPLICATION_JSON)
    .content("""
              {
              "id": 1,
              "title": "title",
              }
        """))
    .andExpect(status().isOk());
  verify(articleRepo, times(1)).findById(1);
  verify(articleRepo, times(1)).updateById(testData.get(0));
  }
}




