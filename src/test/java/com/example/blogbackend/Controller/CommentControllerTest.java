package com.example.blogbackend.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.time.LocalDate;
import java.util.List;
import com.example.blogbackend.Entity.Comment;
import com.example.blogbackend.repository.ArticleRepo;
import com.example.blogbackend.repository.CommentRepo;
import org.junit.jupiter.api.Test;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@WebMvcTest(controllers = CommentController.class)
public class CommentControllerTest {

  @Autowired
  MockMvc mvc;

  @MockBean
  public CommentRepo commentRepo;

  @MockBean
  public ArticleRepo articleRepo;

  List<Comment> testData = List.of(
    new Comment(1, 2, "Jim", "Content1", LocalDate.parse("2021-03-04")),
    new Comment(2, 3, "Ali", "Content2", LocalDate.parse("2021-04-04"))
  );

  @Test
  void testDelete() throws Exception {
    when(commentRepo.delete(1)).thenReturn(true);
    mvc.perform(delete("/comment/1"))
      .andExpect(status().isNoContent());
    verify(commentRepo, times(1)).delete(1);
  }

  @Test
  void testDeleteNotFound() throws Exception {
    when(commentRepo.delete(9)).thenReturn(false);
    mvc.perform(delete("/comment/9"))
      .andExpect(status().isNotFound());
    verify(commentRepo, times(1)).delete(9);
  }

  @Test
  void testFinbByArticleID() throws Exception{
    when(commentRepo.FindByArticleId(1)).thenReturn(testData);
    mvc.perform(get("/comment/article/1"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$").isArray())
      .andExpect(jsonPath("$[1].author").value("Ali"));
    verify(commentRepo, times(1)).FindByArticleId(1);
  }

  @Test
  void testFindAll() throws Exception {
    when(commentRepo.findAll()).thenReturn(testData);
    mvc.perform(get("/comment"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$").isArray())
      .andExpect(jsonPath("$[0].author").value("Jim"));
    verify(commentRepo, times(1)).findAll();

  }

  @Test
  void testFindById() throws Exception {
    when(commentRepo.findById(1)).thenReturn(testData.get(0));
    mvc.perform(get("/comment/1"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.author").value("Jim"))
      .andExpect(jsonPath("$.content").value("Content1"));
    verify(commentRepo, times(1)).findById(1);
  }

  @Test
  void testFindByIdNotFound() throws Exception {
    when(commentRepo.findById(666)).thenReturn(null);
    mvc.perform(get("/comment/666"))
      .andExpect(status().isNotFound());
    verify(commentRepo, times(1)).findById(666);
  }
  
  @Test
  void testSave() throws Exception {
    mvc.perform(post("/comment")
      .contentType("application/json")
      .content("""
              {
                "article_id": 1,
                "author": "Jim",
                "content": "content"
              }
          """))
          .andExpect(status().isCreated());
  }

  @Test
  void testSaveBadRequest() throws Exception {
    mvc.perform(post("/comment")
      .contentType("application/json")
      .content("""
              {
                "article_id": 1,
                "author": "",
                "content": "content"
              }
          """))
          .andExpect(status().isBadRequest());
  }

  @Test
  void testUpdate() throws Exception {
    
  }
}
