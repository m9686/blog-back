package com.example.blogbackend.repository;

import static org.assertj.core.api.Assertions.assertThat;

import com.example.blogbackend.Entity.Article;
import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@Sql({"/DB.sql"})
class ArticleRepoTest {

	@Autowired
	ArticleRepo articleRepo;

	@Test
	void save() {
		Article article = new Article(
				"Les Chats",
				"Michel",
				"Chats",
				LocalDate.parse("2022-10-21")
		);
		articleRepo.save(article);
		assertThat(article.getId()).isNotNull();
	}

	@Test
	void findAll() {
		List<Article> result = articleRepo.findAll();
		assertThat(result)
				.hasSize(3)
				.doesNotContainNull()
				.allSatisfy(article ->
						assertThat(article).hasNoNullFieldsOrProperties()
				);
	}

	@Test
	void findById() {
		Article article = articleRepo.findById(1);
		assertThat(article)
				.hasNoNullFieldsOrProperties()
				.hasFieldOrPropertyWithValue("title", "Les Lapins")
				.hasFieldOrPropertyWithValue("author", "Louis Pelu");
	}
	@Test
	void updateById() {
		Article article = new Article(
				2,
				"Les Chats",
				"Michel",
				"Chats",
				LocalDate.parse("2022-10-21")
		);
		articleRepo.updateById(article);
		article = articleRepo.findById(2);
		assertThat(article)
				.hasNoNullFieldsOrProperties()
				.hasFieldOrPropertyWithValue("title", "Les Chats")
				.hasFieldOrPropertyWithValue("author", "Michel")
				.hasFieldOrPropertyWithValue("content", "Chats");
	}
	@Test
	void delete() {
		articleRepo.delete(1);
		Article result = articleRepo.findById(1);
		assertThat(result).isNull();
	}
}