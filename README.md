# Partie SpringBoot du projet de blog :

[Lien vers le projet frontend](https://gitlab.com/m9686/blogefrontend)

## Structure de l'application :

- Deux entités : Article et Comment qui correspondent aux tables de notre base de données.
- Des Repos correspondant à chacune des entités et comprenant les méthodes principales du CRUD.
- Des Contollers pour chacune des entités, qui défini des chemin d'accès URL pour des application externes, afin de leur donner un accès "controllé" aux données du serveur.
- Des tests pour chacune des parties de l'API enumérées ci-dessus.

## Remarques personnelles :

- Manque de Validation sur les controllers et les entités.
- Les méthodes des repos nesont pas toutes optimisées(Plusieurs requêtes SQL necessaires pour récupérer un seul élément...).
- Les tests de controllers update, save et patch ne fonctionnent pas(sauf quand ils doivent retourner une erreur, là evidemment ils marchent...)

